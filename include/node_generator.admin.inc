<?php

/**
 * @file
 * Admin settings file for node_generator module. 
 */

/**
 * Form constructor for node_generator_input_form form.
 */
function _node_generator_input_form($form, &$form_state) {
  drupal_set_title(t('Node Generator'));
  $form['content_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Content Pattern'),
    '#required' => TRUE,
    '#description' => t('element.classname') . '<strong>(like h1.classname) or element#id(like div#foo)</strong>' . t('For multiple patterns, seperate them by \',\''),
  );
  $form['title_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Title Pattern'),
    '#required' => TRUE,
    '#description' => t('element.classname') . '<strong>(like h1.classname) or element#id(like div#foo)</strong>' . ('For multiple patterns, seperate them by \',\''),
  );
  // Get all content types list.
  $content_types = node_type_get_names();
  foreach ($content_types as $key => $val) {
    // Check if given content type contains body field or not.
    if (!_node_generator_get_content_types($key, 'body')) {
      // If not contain body field, remove that content type from list.
      unset($content_types[$key]);
    }
  }
  $form['select_content_type'] = array(
    '#type' => 'select',
    '#options' => array('' => t('Select')) + $content_types,
    '#title' => t('Select Content Type'),
    '#required' => TRUE,
    '#description' => t('Select content type for which you want to create the node.'),
  );
  $form['category_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Category Pattern'),
    '#required' => FALSE,
    '#description' => t('element.classname') . '<strong>(like h1.classname) or element#id(like div#foo)</strong>' . ('For multiple patterns, seperate them by \',\''),
  );
  $form['category_field'] = array(
    '#type' => 'select',
    '#options' => array('' => t('Select')),
    '#title' => t('Select Field For Category'),
    '#validated' => TRUE,
    '#required' => FALSE,
    '#description' => t('Select Term Refrence Field for Selected Content Type For which you want to create the category.'),
  );
  $form['tag_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Tag Pattern'),
    '#required' => FALSE,
    '#description' => t('element.classname') . '<strong>(like h1.classname) or element#id(like div#foo)</strong>' . ('For multiple patterns, seperate them by \',\''),
  );
  $form['tag_field'] = array(
    '#type' => 'select',
    '#options' => array('' => t('Select')),
    '#title' => t('Select Field For Tags'),
    '#validated' => TRUE,
    '#required' => FALSE,
    '#description' => t('Select Term Refrence Field for Selected Content Type For which you want to create the Tags.'),
  );
  $form['date_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Post Date Pattern'),
    '#required' => FALSE,
    '#description' => t('element.classname') . '<strong>(like h1.classname) or element#id(like div#foo)</strong>' . ('For multiple patterns, seperate them by \',\''),
  );
  $form['upload_file'] = array(
    '#type' => 'managed_file',
    '#title' => t('Upload File'),
    '#required' => FALSE,
    '#description' => t('Allowed Extensions: xml'),
    '#upload_location' => 'public://node_generator/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('xml'),
      'file_validate_size' => array(1 * 1024 * 1024),
    ),
  );
  $form['xml_file_url'] = array(
    '#type' => 'textfield',
    '#title' => t('External URL Path Of XML File.'),
    '#required' => FALSE,
  );
  $form['replace_domain'] = array(
    '#type' => 'checkbox',
    '#prefix' => '<b>Do You Want to Replace Domain.</b>',
    '#default_value' => 0,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['information'] = array(
    '#markup' => '<div><strong>' . t('Note:- Please use pattern only for class or id') . '</strong></div>',
  );
  return $form;
}

/**
 * Form validate for node_generator_input_form form.
 */
function _node_generator_input_form_validate($form, &$form_state) {
  if ($form_state['values']['xml_file_url'] == '' && $form_state['values']['upload_file'] == '') {
    form_set_error('upload_file', t('Please Select Atleast 1 Field.'));
    form_set_error('xml_file_url');
  }
  $check_url = valid_url($form_state['values']['xml_file_url'], $absolute = TRUE);
  if (!$check_url && $form_state['values']['xml_file_url'] != '') {
    form_set_error('xml_file_url', t('Please Enter Valid URL.'));
  }
  if ($form_state['values']['xml_file_url'] != '') {
    $xml = node_generator_get_content_from_xml_loc($form_state['values']['xml_file_url']);
    $validate_xml = simplexml_load_string($xml);
    if (!$validate_xml) {
      form_set_error('xml_file_url', t('Not Valid XML.'));
    }
  }
  if ($form_state['values']['xml_file_url'] == '' && $form_state['values']['upload_file'] != '') {
    $f = file_load($form_state['values']['upload_file']);
    $urll = file_create_url($f->uri);
    $xml = node_generator_get_content_from_xml_loc($urll);
    $validate_xml = simplexml_load_string($xml);
    if (!$validate_xml) {
      form_set_error('edit_upload_file', t('Uploded File Is Not Valid XML.'));
    }
  }
}

/**
 * Submission handler for node_generator_input_form form.
 */
function _node_generator_input_form_submit($form, &$form_state) {
  $batch = array(
    'title' => t('Parsing XML File...'),
    'error_message' => t('An error occurred during processing'),
    'init_message' => t('Creating'),
    'operations' => array(),
    'file' => drupal_get_path('module', 'node_generator') . '/include/node_generator.admin.inc',
    'finished' => '_node_generator_batch_finished',
  );
  if ($form_state['values']['xml_file_url'] != '') {
    $response_xml_data = node_generator_get_content_from_xml_loc($form_state['values']['xml_file_url']);
    if ($response_xml_data) {
      $xml = simplexml_load_string($response_xml_data);
    }
  }
  else {
    $xml = simplexml_load_file(drupal_realpath(file_load($form_state['values']['upload_file'])->uri));
  }
  foreach ($xml as $url) {
    $param = array(
      'url' => (string) $url->loc,
      'type' => $form_state['values']['select_content_type'],
      'content_pattern' => $form_state['values']['content_pattern'],
      'title_pattern' => $form_state['values']['title_pattern'],
      'category_pattern' => $form_state['values']['category_pattern'],
      'tag_pattern' => $form_state['values']['tag_pattern'],
      'date_pattern' => $form_state['values']['date_pattern'],
      'category_field' => $form_state['values']['category_field'],
      'tag_field' => $form_state['values']['tag_field'],
      'replace_domain' => $form_state['values']['replace_domain'],
    );
    $batch['operations'][] = array('_node_generator_processing', array($param));
  }
  // Batch Processing starts here.
  batch_set($batch);
}

/**
 * Process the node generation in for each url and pattern.
 */
function _node_generator_processing($content = array(), &$context) {
  module_load_include('php', 'node_generator', 'include/simple_html_dom');
  $trimed_url = trim($content['url']);
  $html = file_get_html($trimed_url);
  $valid = TRUE;
  $img = node_generator_get_image_from_uri($html, $content['url']);
  $get_anchor_files = node_generator_get_files_from_href($html, $content['url']);
  $category = array();
  foreach (explode(',', $content['category_pattern']) as $pattern_category) {
    foreach ($html->find($pattern_category) as $var_category) {
      $category[] = $var_category->innertext;
    }
  }
  $tag = array();
  foreach (explode(',', $content['tag_pattern']) as $pattern_tag) {
    foreach ($html->find($pattern_tag) as $var_tag) {
      $tag[] = $var_tag->innertext;
    }
  }
  $date = '';
  foreach (explode(',', $content['date_pattern']) as $pattern_date) {
    foreach ($html->find($pattern_date) as $var_date) {
      $date = $var_date->innertext;
      if (!empty($date)) {
        break;
      }
    }
    if (!empty($date)) {
      break;
    }
  }
  // For Metatags.
  $metatags = array();
  $page_title = '';
  foreach (node_generator_get_metatags_url($content['url']) as $key => $val) {
    if (!empty($val) && in_array($key, _node_generator_get_metatags_list())) {

      $metatags['und'][$key]['value'] = $key == 'robots' ? _node_generator_adjust_robot_tags($val) : $metatags['und'][$key]['value'] = $val;
      if ($key == 'title') {
        $page_title = $val;
      }
    }
  }
  // For Body.
  foreach (explode(',', $content['content_pattern']) as $pattern) {
    foreach ($html->find($pattern) as $var) {
      $body = $var->innertext;
      if ($content['replace_domain'] == 1) {
        $body = node_generator_replace_domain_anchor($body, $content['url']);
      }
      if (!empty($body)) {
        break;
      }
    }
    if (!empty($body)) {
      break;
    }
  }
  if (empty($body)) {
    $context['results']['message'][] = t('Unable to create the node for url @url as body is empty', array('@url' => $content['url']));
    $valid = FALSE;
  }
  // For Node Title.
  if ($valid) {
    foreach (explode(',', $content['title_pattern']) as $pattern) {
      foreach ($html->find($pattern) as $var) {
        $node_title = $var->innertext;
        if (!empty($node_title)) {
          break;
        }
      }
      if (!empty($node_title)) {
        break;
      }
    }
    if (empty($node_title)) {
      $context['results']['message'][] = t('Unable to create the node for url @url as node title is empty', array('@url' => $content['url']));
      $valid = FALSE;
    }
  }
  if ($valid) {
    // Generate the node.
    $params = array(
      'url' => $content['url'],
      'uid' => 1,
      'page_title' => $node_title,
      'body' => $body,
      'metatags' => $metatags,
      'type' => $content['type'],
      'alias' => _node_generator_generate_alias($content['url']),
      'summary' => substr($body, 0, 250),
      'category' => $category,
      'tag' => $tag,
      'date' => $date,
      'page-title' => $page_title,
      'category_field' => $content['category_field'],
      'tag_field' => $content['tag_field'],
    );
    _node_generator_node_creation($params, $context);
  }
}

/**
 * Batch process finsh function for node creations.
 */
function _node_generator_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('File parsed successfully') . '<br/>', 'status');
    if (isset($results['message'])) {
      foreach ($results['message'] as $msg) {
        drupal_set_message($msg, 'status');
      }
    }
    if (isset($results['error'])) {
      foreach ($results['error'] as $msg) {
        drupal_set_message($msg, 'error');
      }
    }
  }
  else {
    drupal_set_message(t('File parsed with error.'), 'error');
  }
}

/**
 * Get html data from url using curl.
 */
function node_generator_get_content_from_xml_loc($url) {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}

/**
 * Method to generate metatag list supported by metatag module.
 */
function _node_generator_get_metatags_list() {
  $tags = metatag_get_info();
  $taglist = array();
  foreach ($tags['tags'] as $key => $val) {
    $taglist[] = $tags['tags'][$key]['name'];
  }
  return $taglist;
}

/**
 * Method to adjust the robot metatags.
 */
function _node_generator_adjust_robot_tags($robot_list) {
  $tags = array();
  foreach (explode(',', $robot_list) as $key => $val) {
    $tags[trim($val)] = trim($val);
  }
  return $tags;
}

/**
 * Method to create the new node of specified content type.
 */
function _node_generator_node_creation($content = array(), &$context) {
  $remove_html_title = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $content['page-title']);
  $node = new stdClass();
  $node->uid = $content['uid'];
  $node->type = $content['type'];
  $node->created = time();
  $node->title = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $content['page_title']);
  $node->status = 1;
  $node->promote = 0;
  $node->sticky = 0;
  $node->language = language_default()->language;
  $node->body = array('und' => array(array('value' => $content['body'])));
  $node->body['und'][0]['summary'] = $content['summary'];
  $node->body['und'][0]['format'] = 'full_html';
  $node->metatags = $content['metatags']['und'];
  $node->page_title = $remove_html_title;
  if ($content['category_field'] != '') {
    $tmp_array_cat = array();
    $get_voc_cat = field_info_field($content['category_field']);
    $cat_voc = $get_voc_cat['settings']['allowed_values'][0]['vocabulary'];
    $attachcategory = $content['category_field'];
    $attachtag = $content['tag_field'];
    foreach ($content['category'] as $key => $value) {
      $tmp_array_cat[] = node_generator_get_tid_from_term_name($value, $cat_voc);
    }
    $result = array_unique($tmp_array_cat);
    $cat_attach = array();
    foreach ($result as $key_cat => $value_cat) {
      $cat_attach['und'][] = array('tid' => $value_cat);
    }
    $node->$attachcategory = $cat_attach;
  }
  if ($content['tag_field'] != '') {
    $attachtag = $content['tag_field'];
    $tmp_array = array();
    $get_voc_tag = field_info_field($content['tag_field']);
    $tag_voc = $get_voc_tag['settings']['allowed_values'][0]['vocabulary'];
    foreach ($content['tag'] as $key => $value) {
      $tmp_array[] = node_generator_get_tid_from_term_name($value, $tag_voc);
    }
    $result = array_unique($tmp_array);
    $tag_attach = array();
    foreach ($result as $key_tag => $value_tag) {
      $tag_attach['und'][] = array('tid' => $value_tag);
    }
    $node->$attachtag = $tag_attach;
  }
  $node->created = strtotime($content['date']);
  try {
    node_save($node);
    $context['results']['message'][] = t('Node for @url created having nid @nid', array('@url' => $content['url'], '@nid' => $node->nid));
    if (!drupal_lookup_path('alias', "node/" . $node->nid)) {
      if (!_node_generator_check_alias_exist($content['alias'])) {
        $path = array(
          'source' => 'node/' . $node->nid,
          'alias' => $content['alias'],
          'language' => language_default()->language,
        );
        path_save($path);
      }
      else {
        $context['results']['message'][] = t('Alias already in use. So we haven\'t created/updated alias for node \'@nid\'.', array('@nid' => $node->nid));
      }
    }
  }
  catch (Exception $e) {
    $context['results']['error'][] = t('Unable to create the node for url @url due to some problem. Please check the logs.', array('@url' => $content['url']));
  }
}

/**
 *given content type contains specified field or not.
 */
function _node_generator_get_content_types($bundle, $field) {
  $sql = db_select('field_config_instance', 'fci');
  $sql->fields('fci', array('field_id'));
  $sql->condition('fci.bundle', $bundle);
  $sql->condition('fci.field_name', $field);
  return $sql->execute()->fetchField();
}

/**
 * Method to generate the alias for the given url node.
 */
function _node_generator_generate_alias($url) {
  $substr = substr($url, strpos($url, '//') + 2);
  if (strpos($substr, '/') === FALSE) {
    return '';
  }
  $alias = str_replace(' ', '-', substr($substr, strpos($substr, '/') + 1));
  return $alias;
}

/**
 * Method to check whether given alias already exists or not.
 */
function _node_generator_check_alias_exist($alias) {
  $sql = db_select('url_alias', 'ua');
  $sql->fields('ua', array('pid'));
  $sql->condition('ua.alias', $alias);
  return $sql->execute()->fetchField();
}

/**
 * Download images from remote server to local server.
 */
function node_generator_get_image_from_uri($html, $content) {
  global $base_url;
  foreach ($html->find('img') as $element) {
    $result = parse_url($content);
    $base = $result['scheme'] . "://" . $result['host'];
    $starts_with = substr($element->src, 0, 1);
    if (stripos($element->src, 'http') !== 0) {
      if ($starts_with != '/') {
        $source = $base . '/' . $element->src;
      }
      else {
        $source = $base . $element->src;
      }
    }
    else {
      $source = $element->src;
    }
    $path_parts = pathinfo($element->src);
    $image_name = $path_parts['filename'] . '.' . $path_parts['extension'];
    $destination = 'public://' . $image_name;
    $copy = node_generator_grab_files($source, $destination);
    if ($copy) {
      $uri = 'public://';
      $path_file = file_create_url($uri);
      $p = str_replace($base_url, "", $path_file);
      $element->src = $path_file . $image_name;
    }
  }
}

/**
 * Get tid from existing term name if not exist then create new term.
 */
function node_generator_get_tid_from_term_name($term_name, $vocabulary) {
  $arr_terms = taxonomy_get_term_by_name($term_name, $vocabulary);
  if (!empty($arr_terms)) {
    $arr_terms = array_values($arr_terms);
    $tid = $arr_terms[0]->tid;
  }
  else {
    $vobj = taxonomy_vocabulary_machine_name_load($vocabulary);
    $term = new stdClass();
    $term->name = $term_name;
    $term->vid = $vobj->vid;
    taxonomy_term_save($term);
    $tid = $term->tid;
  }
  return $tid;
}

/**
 * Get metatags from url.
 */
function node_generator_get_metatags_url($url) {
  $metatagss = array();
  $htmll = node_generator_get_content_from_xml_loc($url);
  $doc = new DOMDocument();
  @$doc->loadHTML($htmll);
  $nodes = $doc->getElementsByTagName('title');
  $title = $nodes->item(0)->nodeValue;
  $metas = $doc->getElementsByTagName('meta');
  $description = '';
  $keywords = '';
  for ($i = 0; $i < $metas->length; $i++) {
    $meta = $metas->item($i);
    if ($meta->getAttribute('name') == 'description' || $meta->getAttribute('name') == 'Description')
      $description = $meta->getAttribute('content');
      if ($meta->getAttribute('name') == 'keywords')
        $keywords = $meta->getAttribute('content');
          if ($meta->getAttribute('language') == 'language');
        $language = $meta->getAttribute('language');
  }
  $metatagss['description'] = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $description);
  $metatagss['keywords'] = html_entity_decode($keywords);
  $metatagss['title'] = html_entity_decode($title);
  return $metatagss;
}

/**
 * Get images from url and save them on our server. 
 */
function node_generator_grab_files($url, $saveto) {
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
  $raw = curl_exec($ch);
  curl_close($ch);
  if (file_exists($saveto)) {
    unlink($saveto);
  }
  $fp = fopen($saveto, 'x');
  fwrite($fp, $raw);
  fclose($fp);
  return TRUE;
}

/**
 * Replace absoult anchor paths in body to our server paths. 
 */
function node_generator_replace_domain_anchor($body_html, $url) {
  $result = parse_url($url);
  $base = $result['scheme'] . "://" . $result['host'];
  $base_url = 'href="' . $base . '/';
  $body = str_replace($base_url, 'href="/', $body_html);
  return $body;
}

/**
 * Download images from remote server to local server.
 */
function node_generator_get_files_from_href($html, $content) {
  watchdog('actions', 'Action.', array());
  global $base_url;
  foreach ($html->find('a') as $element) {
    $path_parts = pathinfo($element->href);
    if ($path_parts['extension']) {
      $file_name = $path_parts['filename'] . '.' . $path_parts['extension'];
      if ($path_parts['extension'] == 'pdf' || $path_parts['extension'] == 'doc') {
        $result = parse_url($content);
        $base = $result['scheme'] . "://" . $result['host'];
        $starts_with = substr($element->href, 0, 1);
        if (stripos($element->href, 'http') !== 0) {
          if ($starts_with != '/') {
            $source = $base . '/' . $element->href;
          }
          else {
            $source = $base . $element->href;
          }
        }
        else {
          $source = $element->href;
        }
        $destination = 'public://' . $file_name;
        $copy = node_generator_grab_files($source, $destination);
        if ($copy) {
          $uri = 'public://';
          $path_file = file_create_url($uri);
          $p = str_replace($base_url, "", $path_file);
          $element->href = $path_file . $file_name;
        }
      }
    }
  }
}
